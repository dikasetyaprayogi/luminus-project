<div align="center">

### THE LUMINUS PROJECT

light theme the way it should

<img width="670" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/bannernew.png?ref_type=heads&inline=false">

[<img width="127" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/button2.png?ref_type=heads&inline=false">](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main/docs/extension?ref_type=heads#extension) [<img width="127" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/button1.png?ref_type=heads&inline=false">](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main/docs/extras?ref_type=heads#extras) [<img width="127" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/button3.png?ref_type=heads&inline=false">](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main/docs/design?ref_type=heads#design) [<img width="127" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/button4.png?ref_type=heads&inline=false">](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main/docs/develop?ref_type=heads#develop)

<br> 

**ABOUT**

<p>
a beautiful and elegant light theme alternative for [GNOME](https://www.gnome.org/) <br>
with easy installation & updates through [gnome shell extension](https://extensions.gnome.org/extension/6750/luminus-desktop/) <br> 
that fits nicely with existing [upstream design style](https://developer.gnome.org/hig/) <br> 
</p>

[<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/button-blog.png?ref_type=heads&inline=false">](https://www.buymeacoffee.com/dikasp)

</div>
