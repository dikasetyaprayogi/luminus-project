## EXTRAS

Original wallpaper made for commemorating each major new release.

To create dynamic light/dark wallpaper set, use gnome-tweaks or application like [dynamic wallpaper](https://flathub.org/apps/me.dusansimic.DynamicWallpaper)

#### Light Shell (nostalgia)

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/44/lightshell-thumb.png?ref_type=heads&inline=false">

download: [light](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/44/lightshell-light.png?ref_type=heads&inline=false) [dark](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/44/lightshell-dark.png?ref_type=heads&inline=false)

#### Anew

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/45/anew-thumb.png?ref_type=heads&inline=false">

download: [light](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/45/anew-light.png?ref_type=heads&inline=false) [dark](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/45/anew-dark.png?ref_type=heads&inline=false)

#### Blight

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/46/blight-thumb.png?ref_type=heads&inline=false">

download: [light](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/46/blight-light.png?ref_type=heads&inline=false) [dark](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/46/blight-dark.png?ref_type=heads&inline=false)

#### Claire

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/47/claire-thumb.png?ref_type=heads&inline=false">

download: [light](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/47/claire-light.png?ref_type=heads&inline=false) [dark](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/wallpaper/47/claire-dark.png?ref_type=heads&inline=false)

#### More..

Coming soon..

<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/spacer.png?ref_type=heads&inline=false">

<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/spacer.png?ref_type=heads&inline=false">

[<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/button5.png?ref_type=heads&inline=false">](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main#the-luminus-project)

