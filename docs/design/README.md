## DESIGN

#### Attributes

<img width="700" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/c50b14a372a495e1a4a6b775230bb904.png?ref_type=heads&inline=false">

Luminus Project is using it's own vibrant light blue design called [sunlit theme.](buymeacoffee.com/dikasp/luminus-dedicated-theme-here) This attribute is the default look used everywhere and used whenever possible into existing upstream design.

Our current motto is: simplicity, consistency and beauty.

#### Scope

The goal is not to create a new theme rather to extend and redefine existing light theme that fit nicely with overall upstream design.

#### LSG (Light Styling Guidelines)

Currently we had fine control over gnome shell styling but absolute lack on GNOME applications style due to hard "design" dependency of [libadwaita.](https://news.itsfoss.com/gnome-libadwaita-library/)

With current circumstances, *the base light style of libadwaita is used as reference for light styling and takes first precedence.* An overview of libadwaita library can be accessed on most GNOME system with

`$ adwaita-1-demo`

<img width="700" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/libadwaita-limitation.png?ref_type=heads&inline=false">

This result in some design exception to limit light styling to specific UI for the sake of consistent look with applications, currently:
- dark OSD's, though allowed slight tweaking of it's opacity
- GNOME accents color, handled by the systems (gnome-shell 47+)

<img width="670" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/ext-darkosd.png?ref_type=heads&inline=false">

<img width="670" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/ext-bluecolor.png?ref_type=heads&inline=false">

<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/spacer.png?ref_type=heads&inline=false">

<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/spacer.png?ref_type=heads&inline=false">

[<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/button5.png?ref_type=heads&inline=false">](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main#the-luminus-project)

