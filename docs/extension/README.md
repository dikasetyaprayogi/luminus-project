## EXTENSION

#### Limitation

- Only designed and tested for vanilla GNOME desktop. There might be problem with heavy themed Linux distro like Ubuntu, Manjaro, Pop OS, etc..
- A small number of GNOME apps doesn't support following system theme at all, also some apps may have custom appearance that cant be overridden..
- there still ongoing works to solve compatibility problem with other popular extension eg: dash2dock, dash to panel, just perfection, etc..

#### Installation

Available to browse and install directly from [gnome shell extension website](https://extensions.gnome.org/accounts/profile/dikasp) or via [extensions manager app](https://flathub.org/apps/com.mattjakeman.ExtensionManager)

#### Available Extension

**1. Luminus Desktop**

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/extension1.png?ref_type=heads&inline=false">

Brings global light theme integration for both desktop (gnome shell) and gnome apps (GTK3, GTK4 and later Libadwaita).

This is our main project to bring complete light theme experience as possible that appeals for (now rather declining) light design enthusiast.

**2. Luminus Desktop Y**

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/extension5.png?inline=false">

Custom Luminus Desktop version for use with [Blur my Shell extension](https://extensions.gnome.org/accounts/profile/aunetx) and other transparent theming purpose.

**3. Luminus Shell**

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/extension2.png?ref_type=heads&inline=false">

Replace default gnome shell with light shell, choose this if you want light shell but don't want to force apps into light theme (shell only).

Unlike Luminus Desktop, Luminus Shell follows the [GNOME HIG](https://developer.gnome.org/hig/guidelines/ui-styling.html#light-and-dark-ui-styles) and provides the basic integration while preserving application-specific styles.

**4. Luminus Shell Y**

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/extension3.png?ref_type=heads&inline=false">

Custom Luminus Shell version for use with [Blur my Shell extension](https://extensions.gnome.org/accounts/profile/aunetx) and other transparent theming purpose (shell only).

**5. Light Shell series (transtitional)**

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/extension4.png?ref_type=heads&inline=false">

Former project for GNOME version 45 and below. Later releases continue as dedicated Luminus Project.

<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/spacer.png?ref_type=heads&inline=false">

<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/spacer.png?ref_type=heads&inline=false">

[<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/button5.png?ref_type=heads&inline=false">](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main#the-luminus-project)



