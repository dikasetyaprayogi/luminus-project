## DEVELOP

#### Sources

- `/docs` documentation
- `/src/stylesheet` css sources used to generate light stylesheet
- `/src/extension` build final extension with generated light stylesheet

- `/src/stylesheet/gnome-shell-snapshot` original upstream source snapshot
- `/src/stylesheet/luminus*` our modified sources used to build light stylesheet
- `/src/stylesheet/luminus-shell/` for Luminus Desktop and Luminus Shell extension
- `/src/stylesheet/luminus-shell-y/` for Luminus Desktop Y and Luminus Shell Y extension

See [available extensions](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main/docs/extension?ref_type=heads#available-extension) for Luminus extension variation info.

#### Build

Build depedency:
- sassc: required to compile CSS stylesheet
- gnome-extensions: for working with extensions

the build revolve around simple bash script in [/src](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main/src), the script build light stylesheet from our custom sources then combine it into corresponding Luminus extensions.

- clone the repo
`$ git clone https://gitlab.com/dikasetyaprayogi/luminus-project.git`
- navigate into src folder and make the shell script executeable
`$ cd ./luminus-project-main/src && chmod +x ./*.sh`
- run desired shell script (build.sh must be run once first)
`$ ./build.sh`, `$ ./install.sh`, `$ ./uninstall.sh`

#### Test

Install desired Luminus extension from gnome extension website, extension manager or build and install from source. Installed extension sources are placed in `$HOME/.local/share/gnome-shell/extensions/` on most Linux distro. Then we can do our hack and testing from there, some changes might require logout/relogin to take effect. Reset anytime by reinstalling the extensions or you can also use [virtual nested gnome-shell.](https://gjs.guide/extensions/development/debugging.html#running-a-nested-gnome-shell)

If your gnome-shell version doesn't match the extension version, bypass gnome-extensions version check by modify "shell-version" in `metadata.json` to match yours, after that the extension should be accessible and ready to use.

#### Debug

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/test1.png?ref_type=heads&inline=false">

Our changes are marked as "luminus shell custom" abbreviate as `//lsc ` in css source files. Use a diff program (for example [meld](https://flathub.org/apps/org.gnome.meld) to easily compare and track all changes agaisnt original upstream snapshot in `/src/stylesheet/gnome-shell-snapshot`

<img width="500" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/test2.png?ref_type=heads&inline=false">

After desired changes made, retesting can be done by reusing build script or by using sassc directly to compile light stylesheet and overwrite installed Luminus extension ones, for example:
`$ sassc ./luminus-project/src/stylesheet/luminus-shell/data/theme/gnome-shell-light.scss > /home/user/.local/share/gnome-shell/extensions/luminus-shell@dikasp.gitlab/stylesheet-light.css`
apply the changes by toggling the extension on/off, reset anytime by reinstalling or build fresh Luminus extension. Some changes might require logout/relogin to take effect.

#### Development

New extension build are worked out when there is major new gnome shell release or If there is a need for important bug fixes, and also when the developers had time to do it - so there no fixed schedule.

on gnome extension website, the extension use upstream gnome shell version and if there are multiple revision the number is incremented, ex:
- 47.alpha - gnome shell v47 (prerelease) with Luminus testing build
- 47 - gnome shell v47.0 (stable release) with Luminus build
- v47.2.v2 - gnome shell v47.2 (point release) with 2nd revision Luminus build

the git branch only hold major gnome-shell releases (v45, v46, v47..) and always get updated with latest revision, minor and testing revision (v45-alpha, v45.1, v45.2..) are archived [here](https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/tree/main/etc/backup?ref_type=heads)

Usual development cycle including:
1. Syncing newest [upstream sources,](https://gitlab.gnome.org/GNOME/gnome-shell/-/tree/main/data/theme?ref_type=heads) `/data/theme` is the only needed files
2. Making changes into sources based on [design guidelines](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main/docs/design?ref_type=heads#design)
3. Test/debug, also test against reported [issues](https://gitlab.com/dikasetyaprayogi/luminus-project/-/issues)
4. Final extension builds then get published into [extensions gnome org](https://extensions.gnome.org/upload/)
5. Merge stable changes into main branch

For insight of current plan and future ideas for Luminus see [luminus-todo](https://gitlab.com/dikasetyaprayogi/luminus-project/-/issues/10)

<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/spacer.png?ref_type=heads&inline=false">

<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/spacer.png?ref_type=heads&inline=false">

[<img width="186" src="https://gitlab.com/dikasetyaprayogi/luminus-project-archive/-/raw/main/assets/47/button5.png?ref_type=heads&inline=false">](https://gitlab.com/dikasetyaprayogi/luminus-project/-/tree/main#the-luminus-project)
