#!/bin/bash

echo "Checking dependency.."

if ! [ -x "$(command -v sassc)" ]; then
  echo 'Error: sassc program not found, install it first on your system.' >&2
  exit 1
fi

echo "Generating files into _build folder.."

mkdir _build

# Build light stylesheet
sassc ./stylesheet/luminus-shell/data/theme/gnome-shell-light.scss > ./extension/luminus-desktop@dikasp.gitlab/stylesheet-light.css
sassc ./stylesheet/luminus-shell/data/theme/gnome-shell-light.scss > ./extension/luminus-shell@dikasp.gitlab/stylesheet-light.css
sassc ./stylesheet/luminus-shell-y/data/theme/gnome-shell-light.scss > ./extension/luminus-desktop-y@dikasp.gitlab/stylesheet-light.css
sassc ./stylesheet/luminus-shell-y/data/theme/gnome-shell-light.scss > ./extension/luminus-shell-y@dikasp.gitlab/stylesheet-light.css

# Build xtensions
zip -q -j ./_build/luminus-desktop.zip ./extension/luminus-desktop@dikasp.gitlab/*
zip -q -j ./_build/luminus-shell.zip ./extension/luminus-shell@dikasp.gitlab/*
zip -q -j ./_build/luminus-desktop-y.zip ./extension/luminus-desktop-y@dikasp.gitlab/*
zip -q -j ./_build/luminus-shell-y.zip ./extension/luminus-shell-y@dikasp.gitlab/*

echo "Done."
