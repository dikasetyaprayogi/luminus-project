#!/bin/bash

echo "Checking dependency.."

if ! [ -x "$(command -v gnome-extensions)" ]; then
  echo 'Error: gnome-extensions program not found, install it first on your system.' >&2
  exit 1
fi

echo "Installing Luminus extensions.."

gnome-extensions install --force ./_build/luminus-desktop.zip
gnome-extensions install --force ./_build/luminus-shell.zip
gnome-extensions install --force ./_build/luminus-desktop-y.zip
gnome-extensions install --force ./_build/luminus-shell-y.zip

echo "Done. Logout might required for the changes to take effect."
