#!/bin/bash

echo "Checking dependency.."

if ! [ -x "$(command -v gnome-extensions)" ]; then
  echo 'Error: gnome-extensions program not found, install it first on your system.' >&2
  exit 1
fi

echo "Removing builds and uninstalling Luminus extensions"
rm -r ./_build

gnome-extensions disable luminus-desktop@dikasp.gitlab
gnome-extensions disable luminus-shell@dikasp.gitlab
gnome-extensions disable luminus-desktop-y@dikasp.gitlab
gnome-extensions disable luminus-shell-y@dikasp.gitlab

gnome-extensions uninstall luminus-desktop@dikasp.gitlab
gnome-extensions uninstall luminus-shell@dikasp.gitlab
gnome-extensions uninstall luminus-desktop-y@dikasp.gitlab
gnome-extensions uninstall luminus-shell-y@dikasp.gitlab

echo "Done. Logout might required for the changes to take effect."
